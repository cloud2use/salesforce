amazon2use

This is a minimal python package, for use alongside the lesson: https://github.com/cloud2use/amazon

Contributing:

Please do! Guidelines:

 - This lesson is for beginners. Yes there are plenty of super fancy things we don't cover here; someday, we'll make a different, super fancy lesson to teach those things.
 - Max 100 lines per PR please.
 - Open an issue or empty PR first to talk about controversial changes.

